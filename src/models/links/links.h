#ifndef LINKS_H
#define LINKS_H

#include <QObject>
#ifdef STATIC_MAUIKIT
#include "fmh.h"
#include "mauilist.h"
#else
#include <MauiKit/fmh.h>
#include <MauiKit/mauilist.h>
#endif

#include "owl.h"

class DB;
class Links : public MauiList
{
    Q_OBJECT
    Q_PROPERTY(SORTBY sortBy READ getSortBy WRITE setSortBy NOTIFY sortByChanged)
    Q_PROPERTY(ORDER order READ getOrder WRITE setOrder NOTIFY orderChanged)

public:
    enum ORDER : uint8_t
    {
        DESC,
        ASC
    };
    Q_ENUM(ORDER)

    enum SORTBY : uint8_t
    {
        TITLE = FMH::MODEL_KEY::TITLE,
        ADDDATE = FMH::MODEL_KEY::ADDDATE,
        MODIFIED = FMH::MODEL_KEY::MODIFIED,
        COLOR = FMH::MODEL_KEY::COLOR,
        FAVORITE = FMH::MODEL_KEY::FAVORITE,
        PIN = FMH::MODEL_KEY::PIN
    };
    Q_ENUM(SORTBY)

    explicit Links(QObject *parent = nullptr);
    FMH::MODEL_LIST items() const override final;

    void setSortBy(const SORTBY &sort);
    SORTBY getSortBy() const;
    void setOrder(const ORDER &order);
    ORDER getOrder() const;

private:
    DB *db;
    FMH::MODEL_LIST links;
    void sortList();

    SORTBY sort = SORTBY::MODIFIED;
    ORDER order = ORDER::DESC;

signals:
    void orderChanged();
    void sortByChanged();

public slots:
    QVariantMap get(const int &index) const;
    bool insert(const QVariantMap &link);
    bool update(const QVariantMap &data, const int &index);
    bool remove(const int &index);
    static const QString previewsCachePath()
    {
        return OWL::LinksPath.toLocalFile();
    }
};

#endif // NOTES_H
